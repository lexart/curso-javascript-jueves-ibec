var contactCtrl 	= function () {
	console.log("contact ::");
	var contContact = $('#contact');
	var childViews 	 = "contact/views/";
	var req  		 = $.get(VIEWS + childViews + "contactView.html");

	req.then( function (data) {

		var dataHtml = $(data).appendTo('#contact');
		
		var titulo 	 	= contContact.find('p');
		var link 		= contContact.find('a');
		var form 		= contContact.find('form');

		// MODEL
		var mdl = {
			p:{
				titulo: "CONTACT"
			},
			a:{
				titulo: "Contactar",
				link: "mailTo:lexcasa@gmail.com"
			}
		};

		titulo.text(mdl.p.titulo);
		link.text(mdl.a.titulo);
		link.attr('href', mdl.a.link);

		
		form.submit( function (e) {
			var asunto = $(this).find('#asunto').val();
			var nombre = $(this).find('#nombre').val();

			if(!asunto){
				alert("Debe especificar asunto");
				return false;
			}
			if(!nombre){
				alert("Debe especificar nombre");
				return false;
			}
			
			var postData = {
				asunto: asunto,
				nombre: nombre
			};

			$.post( API + 'post/index.php', JSON.stringify(postData) ).then( function (res) {
				console.log("post res: ", res);
			});
			console.log("asunto: ", asunto, nombre)
			return false;
		});
	});
};