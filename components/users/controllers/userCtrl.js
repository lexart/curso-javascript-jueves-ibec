var userCtrl 	= function () {
	console.log("contact ::");
	var contUser = $('#users');
	var childViews 	 = "users/views/";
	var req  		 = $.get(VIEWS + childViews + "userView.html");

	req.then( function (data) {

		var dataHtml = $(data).appendTo('#users');
		
		var titulo 	 	= contUser.find('p');
		var link 		= contUser.find('a');
		var form 		= contUser.find('form');

		var table		= contUser.find('#usuarios');
		var tbody		= table.find('tbody');

		var usersArr    = [
			{
				id: 123,
				nombre: "Alex",
				email: "Casadevall",
				clave: "12312"
			},
			{
				id: 222,
				nombre: "Juan",
				email: "Casadevall",
				clave: "12312"
			}
		];

		if(window.localStorage['users']){
			usersArr = JSON.parse(window.localStorage['users']);
		}

		// LISTAR

		for (var i = usersArr.length - 1; i >= 0; i--) {
			tbody.append('<tr><th>'+usersArr[i].id+'</th><th>'+usersArr[i].nombre+'</th><th>'+usersArr[i].email+'</th><th><a id="editar-'+usersArr[i].id+'" class="editar" href="#">Editar</a></th></tr>');
		};


		var saveUser 	= function  () {
			form.submit( function () {
				var id 	   = $(this).find('#id').val();
				var nombre = $(this).find('#nombre').val();
				var email = $(this).find('#email').val();
				var clave = $(this).find('#clave').val();

				var user  = {
					id: Math.floor((Math.random() * 1000) + 1),
					nombre: nombre,
					email: email,
					clave: clave
				};

				console.log("user: ", user);

				if(!id){
					usersArr.push(user);
				} else {
					user.id = id;
					for (var i = usersArr.length - 1; i >= 0; i--) {
						if(usersArr[i].id == id){
							usersArr[i] = user;
							break;
						}
					};
				}
				


				tbody.find('tr').remove();

				// BUILD TABLE
				for (var i = usersArr.length - 1; i >= 0; i--) {
					tbody.append('<tr><th>'+usersArr[i].id+'</th><th>'+usersArr[i].nombre+'</th><th>'+usersArr[i].email+'</th><th><a id="editar-'+usersArr[i].id+'" class="editar" href="#">Editar</a></th></tr>');
				};

				window.localStorage['users'] = JSON.stringify(usersArr);
				
				editUser();
				
				return false;
			})
		};
		saveUser();

		var editUser = function () {
			console.log("edit allow");
			var edit = tbody.find('.editar');
			edit.click( function () {
				var id = $(this).attr('id').split('-')[1];

				console.log("id edit: ", id);
				for (var i = usersArr.length - 1; i >= 0; i--) {
					if(usersArr[i].id == id){
						var user = usersArr[i];

						form.find('#id').val(user.id);
						form.find('#nombre').val(user.nombre);
						form.find('#email').val(user.email);
						form.find('#clave').val(user.clave);

						break;
					}
				};
			})
		};
		editUser();

		// MODEL
		var mdl = {
			p:{
				titulo: "USUARIOS"
			},
			a:{
				titulo: "Contactar",
				link: "mailTo:lexcasa@gmail.com"
			}
		};

		titulo.text(mdl.p.titulo);
	});
};