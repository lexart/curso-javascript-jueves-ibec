var homeCtrl 	= function () {
	console.log("home ::");

	var contHome 	 = $('#home');
	var res 	 	 = [{name:"agua"}];
	var childViews 	 = "home/views/";
	var req  		 = $.get(VIEWS + childViews + "homeView.html");

	req.then( function (data) {

		var dataHtml = $(data).appendTo('#home');
		var titulo 	 = contHome.find('p');
		var footer 	 = contHome.find('footer');
		var items    = contHome.find('.submenu li');
		var btn 	 = contHome.find('button');

		console.log("titulo: ", titulo);
		// MODEL
		var mdl = {
			p:{
				titulo: "HOME"
			},
			submenu: [
				{
					titulo:"item 1"
				},
				{
					titulo:"item 2"
				},
				{
					titulo:"item 3"
				}
			],
			footer:{
				titulo: "Estamos en la HOME.",
			},
			btn: function (){
				titulo.text("Home - Hola mundo JS");
				$(this).text("Clicked!");
			}
		};
		
		for (var i = items.length - 1; i >= 0; i--) {
			$(items[i]).text(mdl.submenu[i].titulo);
		};

		titulo.text(mdl.p.titulo);
		footer.text(mdl.footer.titulo);
		btn.click(mdl.btn);
	});
};