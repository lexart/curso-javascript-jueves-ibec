var loginCtrl 	= function () {
	console.log("login ::");
	var contContact = $('#login');
	var childViews 	 = "login/views/";
	var req  		 = $.get(VIEWS + childViews + "loginView.html");

	req.then( function (data) {

		var dataHtml = $(data).appendTo('#login');
		
		var titulo 	 	= contContact.find('p');
		var link 		= contContact.find('a');
		var form 		= contContact.find('form');

		
		form.submit( function (e) {
			var asunto = $(this).find('#email').val();
			var nombre = $(this).find('#clave').val();

			if(!asunto){
				alert("Debe especificar email");
				return false;
			}
			if(!nombre){
				alert("Debe especificar clave");
				return false;
			}
			
			var postData = {
				asunto: asunto,
				nombre: nombre
			};

			$.post( API + 'post/index.php', JSON.stringify(postData) ).then( function (res) {
				console.log("post res: ", res);
				if(res){
					window.localStorage['validUser'] = true;
					window.location.href='?ruteo=home';
				}
			});
			console.log("asunto: ", asunto, nombre)
			return false;
		});
	});

	$('#menu').hide();
};