// CALL CTRLS

var secciones = $('.secciones');
var ENDPOINT  = "https://restcountries.eu/rest/v2/all";
var API 	  = "api/";
var VIEWS	  = "components/";

menuCtrl();

console.log("ruteo url: ", ruteo);

if(!window.localStorage['validUser'] && !window.location.href.includes('login')){
	window.location.href="?ruteo=login";
}

if(ruteo == "home"){
	homeCtrl();
}
if(ruteo == "contact"){
	contactCtrl();
}
if(ruteo == "news"){
	newsCtrl();
} 
if(ruteo == "login"){
	loginCtrl();
}
if(ruteo == "users"){
	userCtrl();
} 
